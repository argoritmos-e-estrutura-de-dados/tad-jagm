package pratica06;

/** @author messiah */
public class main {
    public static void main(String[] args) {
        System.out.println("Grafo 1:");
        JGraph jg_0 = new JGraph(9);        
        jg_0.insertEdge(0, 1, 5);
        jg_0.insertEdge(0, 3, 10);
        jg_0.insertEdge(0, 4, 5);
        jg_0.insertEdge(0, 6, 10);
        jg_0.insertEdge(0, 7, 10);
        jg_0.insertEdge(1, 0, 5);
        jg_0.insertEdge(1, 5, 5);
        jg_0.insertEdge(1, 7, 5);
        jg_0.insertEdge(1, 8, 10);
        jg_0.insertEdge(2, 3, 5);
        jg_0.insertEdge(2, 4, 10);
        jg_0.insertEdge(2, 6, 5);
        jg_0.insertEdge(3, 0, 10);
        jg_0.insertEdge(3, 2, 5);
        jg_0.insertEdge(3, 4, 5);
        jg_0.insertEdge(3, 6, 10);
        jg_0.insertEdge(4, 0, 5);
        jg_0.insertEdge(4, 2, 10);
        jg_0.insertEdge(4, 3, 5);
        jg_0.insertEdge(4, 8, 10);
        jg_0.insertEdge(5, 1, 15);
        jg_0.insertEdge(5, 6, 15);
        jg_0.insertEdge(6, 0, 10);
        jg_0.insertEdge(6, 2, 5);        
        jg_0.insertEdge(7, 1, 5);
        jg_0.insertEdge(7, 8, 20);
        jg_0.insertEdge(7, 3, 10);
        jg_0.insertEdge(7, 5, 15);
        jg_0.insertEdge(8, 1, 10);
        jg_0.insertEdge(8, 4, 10);
        jg_0.insertEdge(8, 7, 20);
        jg_0.print();
        
        JAGM jAGM_0 = new JAGM(8, jg_0);
        System.out.println("Árvore Geradora mínima: ");
        jAGM_0.search().print();
                       
        System.out.println("\nGrafo 2: ");        
        JGraph jg_1 = new JGraph(10);
        jg_1.insertEdge(1, 2, 8);
        jg_1.insertEdge(1, 3, 2);
        jg_1.insertEdge(1, 7, 9);
        jg_1.insertEdge(2, 1, 8);
        jg_1.insertEdge(2, 3, 3);
        jg_1.insertEdge(2, 4, 2);
        jg_1.insertEdge(2, 5, 7);
        jg_1.insertEdge(2, 6, 5);
        jg_1.insertEdge(2, 8, 6); 
        jg_1.insertEdge(3, 1, 2); 
        jg_1.insertEdge(3, 2, 3); 
        jg_1.insertEdge(3, 4, 9); 
        jg_1.insertEdge(3, 7, 6);
        jg_1.insertEdge(4, 2, 2);
        jg_1.insertEdge(4, 3, 9); 
        jg_1.insertEdge(4, 5, 6); 
        jg_1.insertEdge(4, 7, 2); 
        jg_1.insertEdge(5, 2, 7); 
        jg_1.insertEdge(5, 3, 6); 
        jg_1.insertEdge(5, 6, 5); 
        jg_1.insertEdge(5, 7, 7); 
        jg_1.insertEdge(6, 2, 5); 
        jg_1.insertEdge(6, 5, 5); 
        jg_1.insertEdge(6, 7, 3); 
        jg_1.insertEdge(6, 8, 4); 
        jg_1.insertEdge(7, 1, 9); 
        jg_1.insertEdge(7, 3, 6); 
        jg_1.insertEdge(7, 4, 2); 
        jg_1.insertEdge(7, 5, 4); 
        jg_1.insertEdge(7, 6, 6); 
        jg_1.insertEdge(7, 8, 3); 
        jg_1.insertEdge(8, 2, 6); 
        jg_1.insertEdge(8, 6, 4); 
        jg_1.insertEdge(8, 7, 3); 
        jg_1.print();
        
        JAGM jAGM_1 = new JAGM(10, jg_1);
        System.out.println("Árvore Geradora mínima: ");
        jAGM_1.search().print();
    }
    
}