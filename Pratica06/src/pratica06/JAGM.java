package pratica06;

/** O algoritmo apresentado foi de autoria de Filipe Saraiva (filip.saraiva@gmail.com) 
disponível em https://github.com/hacktoon/1001/blob/master/other-languages/graphs/prim/prim.java  */

public class JAGM {
    private JGraph jg;
    private int already_visited[], n;
    
    
    private enum COLORS{
        WHITE, BLACK;
    }
    
    public JAGM(int n, JGraph jg){
        this.already_visited = new int[n];
        this.jg = jg;
        this.n = n;
    }
    
    /** Implementação O(n²) do algoritmo de Prim para Árvore Geradora Mínima (AGM). */
    public JGraph search(){
        /** Armazena os vértices já verificados.. */
        boolean verified_vertices[] = new boolean[n];
        /** Armazena as distâncias relativas para cada vértice em cada iteração. */
        int relative_distance[] = new int[n];
        /** Armazena os nós vizinhos de cada nó do grafo da árvore final produzida. */
        int empty_nodes[] = new int[n];
        /** Armazena o peso total da árvore geradora mínima. */
        int total_weight = 0;
        /** Inicialização de variáveis. */
        for(int i=0; i<n; i++){
            verified_vertices[i] = false;
            relative_distance[i] = 99;
            empty_nodes[i] = 0;
        }
        relative_distance[0] = 0;
        /** Define a a raiz da árvore resultante. */
        int rated_point = 0;        
        for(int i=0; i<n; i++){
            for(int j=0; j<n; j++){
                /** Verifica se o vertice já foi avaliado anteriormente; se sim, 
                prossegue para a próxima iteração. */
                if(verified_vertices[j] || j == rated_point) continue;
                /** Verifica se há uma aresta com peso maior que 0 entre os vértices
                i e j do grafo e se este é menor que a atual distanciaRelativa do nó 
                vizinho; Caso sim, a distanciaRelativa do nó vizinho ao que está sendo
                avaliado no momento será atualizada pelo valor dessa nova distancia 
                avaliada até o pontoAvaliado. */
                if((jg.getE()[i][j] > 0) && jg.getE()[i][j] < relative_distance[j]){
                    relative_distance[j] = jg.getE()[i][j];
                    empty_nodes[j] = rated_point;
                }
            }
            /** Define que o vértice de pontoAvaliado como já verificado. */
            verified_vertices[rated_point] = true;
            /**  o valor as variavéis para a seleção do próximo vértice a ser avaliado. */
            rated_point = 0;
            int comparative_distance = 99;
            /** Seleção do próximo vértice a ser avaliado. */
            for(int j=0; j<n; j++){
                /** Verifica se o vertice já foi avaliado anteriormente; se sim, 
                prossegue para a próxima iteração. */
                if(verified_vertices[j]) continue;                
                /** Se a distância relativa desse ponto for menor que a do ponto avaliado
                assume-se esse novo ponto como o ponto avaliado; Ao final da iteração, será
                selecionado o ponto com menor distância relativa. */
                if(relative_distance[j] < comparative_distance){
                    comparative_distance = relative_distance[j];
                    rated_point = j;
                }
            }
        }
        /** Criação da matrizResposta com a árvore resultante do Algoritmo de Prim. */
        JGraph jg_result = new JGraph(n);
        for(int i=0; i<n; i++){
            int weight = jg.getE()[i][empty_nodes[i]];
            jg_result.insertEdge(i, empty_nodes[i], weight);
            jg_result.insertEdge(empty_nodes[i], i, weight);
            total_weight += weight;
        }
        System.out.println("Peso total: "+total_weight);
        return jg_result;
    }    
}